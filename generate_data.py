#!/usr/bin/env python

import urllib2, json, os, sys, errno, argparse, random, decimal
from pprint import pprint

##### SETUP COMMAND LINE PARSER #####

parser = argparse.ArgumentParser(description='This script generates demo data is JSON format for SlamData to use.')
parser.add_argument('-p','--patients', default=10000, type=int, help='Number of patients (defaults to 10000)')
parser.add_argument('-o','--output', help='Output file name (defaults to stdout)')
parser.add_argument('-v','--verbose', help='Verbose output', action='store_true')
parser.add_argument('-q','--quiet', help='Silent operation except for failure')
args=parser.parse_args()

cwd = os.getcwd()

##################
# GLOBAL VARIABLES

srcdir = cwd + "/srcdata"
states_dir = srcdir + "/states/"
names_dir = srcdir + "/names/"
streets_file = srcdir + "/streets.txt"

female_first_names		= []
female_middle_names		= []
male_first_names		= []
male_middle_names		= []
last_names				= []

PATIENTS = []

USED_SSNS = []

STREETS_LIST = []

STATES_LIST=['AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY']

STATES_DICT = dict()

STATE_URL="http://gomashup.com/json.php?fds=geo/usa/zipcode/state/"
# example http://gomashup.com/json.php?fds=geo/usa/zipcode/state/TX


# Systolic / Dialostic
BP_MIN = [80, 60]
BP_GOOD = [120, 80]
BP_STAGE1 = [139, 89]
BP_STAGE2 = [159, 99]
BP_STAGE3 = [180, 110]

BP_STAGES = [BP_MIN, BP_GOOD, BP_STAGE1, BP_STAGE2, BP_STAGE3]

################################


def get_random_height():

	weighted_choices = [(60,2),(62,5),(63,10),(64,15),(65,20),(66,25),(67,30),(68,25),(69,20),(70,15),(71,10),(72,5)]
	range_height = [val for val, cnt in weighted_choices for i in range(cnt)]
	height = random.choice(range_height)
	above = random.randint(0,1)
	if above:
		height = height + (random.randint(0,12))
	else:
		height = height - (random.randint(0,12))

	return height;


def get_random_weight(gender, age, height):

	ideal = get_ideal_weight(gender, age, height)

	weighted_choices = [(80,2),(90,5),(100,10),(110,20),(120,25),(130,30),(140,20),(150,20),(160,15),(170,10),(180,5),(190,4),(220,2),(210,2)]
	multiplier = [val for val, cnt in weighted_choices for i in range(cnt)] 
	actual_multiplier = (random.choice(multiplier))
	weight = ideal * (actual_multiplier * .01)
	# We have a standard weight deviation, now add or subtract 0-10 pounds for randomness
	above = random.randint(0,1)
	if above:
		weight = weight + (random.randint(0,10))
	else:
		weight = weight - (random.randint(0,10))

	dec = decimal.Decimal(weight)
	weight = round(weight,0)
	return weight


def get_ideal_weight(gender, age, height):

	# height should be in inches
	# gender is 'male' or 'female'

	# Hamwi method of ideal weight:
	# MEN: 106 pounds plus 6 pounds for every inch over 5 feet.
	# WOM: 100 pounds plus 5 pounds for every inch over 5 feet.

	pounds_per_inch = 0
	overage = height - 60   # Calculate inches over 5 feet

	if age < 16:

		ideal = (age + 4) * 2  # APLS Method in kg
		ideal = ideal * 2.2046 # Multiple for pounds

	else:

		if gender == 'male':

			base = 106
			pounds_per_inch = 6

		else:

			base = 100
			pounds_per_inch = 5

		ideal = base + ( overage * pounds_per_inch )

	return ideal


def get_random_ssn():

	# Returns a string in format of 123-45-6789

	# originally returned only unique SSNs but
	# as the number of patients grew bigger, the
	# time grew dramatically.  At 100,000 patients
	# it went from 6s to 1m3s

	ssn_found = False

	ssn = ""
	char = 0

	while char < 11:

		char = char + 1

		if char == 4 or char == 7:
			ssn = ssn + "-"

		else:
			ssn = ssn + str(random.randint(0,9))

	return ssn


def get_previous_addresses():

	num_addresses = random.randint(0,5)

	addresses = []

	for address_count in range(num_addresses):

		addresses.append(get_random_location())

	return addresses


def get_random_location():

	return_location = dict()

	# To make sure we get an entry with all valid
	# information, we may cycle through a few times

	found = False
	count = 0

	while not found:

		count = count + 1
		state = random.choice(STATES_LIST)
		#print STATES_DICT[state]
		zip_code = random.choice(STATES_DICT[state].keys())
		tmp_data = STATES_DICT[state][zip_code]
		if tmp_data['city']:
			if tmp_data['county']:
				if tmp_data['longitude']:
					if tmp_data['latitude']:
						return_location['state'] = state
						return_location['zip_code'] = zip_code
						return_location['city'] = tmp_data['city']
						return_location['county'] = tmp_data['county']
						return_location['longitude'] = tmp_data['longitude']
						return_location['latitude'] = tmp_data['latitude']
						found = True

	return return_location


def generate_patients(num_patients):

	if args.verbose: print "Generating data for %d patients..." % num_patients

	for num in range(num_patients):
		ssn = get_random_ssn()
		age = get_random_age()
		gender = get_random_gender()
		height = get_random_height()
		weight = get_random_weight(gender, age, height)
		street_address = get_random_street_address()
		my_full_name = get_random_name(gender)
		first_name = my_full_name[0]
		middle_name = my_full_name[1]
		last_name = my_full_name[2]
		location = get_random_location()
		state = location['state']
		zip_code = location['zip_code']
		city = location['city']
		county = location['county']
		longitude = location['longitude']
		latitude = location['latitude']

		if args.verbose:
			print "SSN     :" + str(ssn)
			print "Name    :" + first_name + " " + middle_name + " " + last_name
			print "Address :" + street_address
			print "        :" + city + "," + state + " " + zip_code
			print "County  :" + county
			print "Longitud:%f" % longitude
			print "Latitude:%f" % latitude
			print "Gender  :" + gender
			print "Age     :" + str(age)
			print "Height  :" + str(height)
			print "Weight  :" + str(weight)
			print "==============="

		previous_addresses = get_previous_addresses()

		td = dict()
		td['ssn'] = ssn
		td['age'] = age
		td['gender'] = gender
		td['weight'] = weight
		td['height'] = height
		td['street_address'] = street_address
		td['first_name'] = my_full_name[0]
		td['middle_name'] = my_full_name[1]
		td['last_name'] = my_full_name[2]
		td['state'] = location['state']
		td['zip_code'] = location['zip_code']
		td['city'] = location['city']
		td['county'] = location['county']
		td['loc'] = [location['longitude'],location['latitude']]
		td['previous_addresses'] = previous_addresses

		#td['longitude'] = location[4]
		#td['latitude'] = location[5]

		PATIENTS.append(td)

		if num % 1000 == 0:
			sys.stdout.write('.')
			sys.stdout.flush()

	if args.output:

		if os.path.isfile(str(args.output)):

			# If the output file already exists, remove
			# and open a handle to the new one

			if args.verbose: print "Output file %s exists, removing" % str(args.output)
			os.remove(str(args.output))

		if args.verbose: print "Writing to %s" % str(args.output)


		with open(str(args.output), 'w') as jsonfile:

			for entry in PATIENTS:

				jsonfile.write(json.dumps(entry) + "\n")

		jsonfile.close

	else:

		for entry in PATIENTS:

			print json.dumps(entry)

		
def get_random_street_address():
	address_number = random.randint(0,9999)
	street_name = get_random_street()
	return str(address_number) + " " + street_name


def get_random_name(gender_type):

	first = ""
	middle = ""
	last = ""

	if gender_type == "male":

		first = random.choice(male_first_names)
		middle = random.choice(male_middle_names)

	if gender_type == "female":

		first = random.choice(female_first_names)
		middle = random.choice(female_middle_names)

	last = random.choice(last_names)
	full_name = [first, middle, last]
	return full_name

def get_random_street():

	return random.choice(STREETS_LIST)



def get_random_gender():

	weighted_choices = [('male', 49),('female', 51)]
	choices = [val for val, cnt in weighted_choices for i in range(cnt)]
	gender = random.choice(choices)
	return gender


def get_random_age():
	# Don't do less than 20 because it's hard to estimate height for kids
	weighted_choices = [(2,25),(3,30),(4,10),(5,25),(6,15),(7,10),(8,5),(9,2)]
	tens = [val for val, cnt in weighted_choices for i in range(cnt)]
	age = random.choice(tens) * 10
	age += random.randint(0,9)
	return age


def get_average_age(age_list):

	return sum(age_list)/len(age_list)


def load_names(file_name):

	if args.verbose: print "Loading %s" % file_name
	name_list = []
	with open(file_name) as f:
		for line in f:
			# remove CR/LF
			name_list.append(line[0:-1])
	return name_list


def load_streets():

	if args.verbose:
		print "LOADING STREETS"

	with open(srcdir + '/streets.txt') as f:
		for line in f:
			actual_line = line.strip('\n')
			STREETS_LIST.append(actual_line)


def count_male_names():
	return len(male_first_names) * len(male_middle_names) * len(last_names)


def count_female_names():
	return len(female_first_names) * len(female_middle_names) * len(last_names)


def load_all_names():

	if args.verbose:
		print "NAME DATA\n========="

	global female_first_names
	global female_middle_names
	global male_first_names
	global male_middle_names
	global last_names

	female_first_names = load_names(names_dir + "female_first_names.txt")
	female_middle_names = load_names(names_dir + "female_middle_names.txt")
	male_first_names = load_names(names_dir + "male_first_names.txt")
	male_middle_names = load_names(names_dir + "male_middle_names.txt")
	last_names = load_names(names_dir + "last_names.txt")

	if args.verbose:
		print "Female first names  :%d" % len(female_first_names)
		print "Female middle names :%d" % len(female_middle_names)
		print "Male first names    :%d" % len(male_first_names)
		print "Male middle names   :%d" % len(male_middle_names)
		print "Last names          :%d" % len(last_names)


def fetch_state(state):

	if args.verbose: print "Fetching state %s from Internet" % state
	url = STATE_URL + state
	raw_response = urllib2.urlopen(url).read()
	# remove leading and trailing parens...
	json_response = json.loads(raw_response[1:-1])
	json_data = json_response['result']
	
	# convert long/lat to floats and zipcode to int

	for entry in json_data:
		# Sometimes these are empty strings, so check first
		if entry['Longitude']:
			entry['Longitude'] = float(entry['Longitude'])
		if entry['Latitude']:
			entry['Latitude'] = float(entry['Latitude'])
		if entry['Zipcode']:
			entry['Zipcode'] = int(entry['Zipcode'])

	file_name = states_dir + str(state) + ".json"
	with open(file_name, 'w') as jsonfile:
		jsonfile.write(json.dumps(json_data, indent=4, sort_keys=True))
		jsonfile.close


def load_state(state):

	state_file = states_dir + str(state) + ".json"

	if not os.path.isfile(state_file):
		print "Missing state %s, fetching from Internet..." % state
		fetch_state(state)

	with open(state_file) as json_file:
		json_data = json.load(json_file)
		json_file.close

	# convert the json from the file into something
	# more structured and pass it back

	state_json = []

	for entry in json_data:

		zip_code = None
		missing_data = False

		tmp_data = dict()
		if entry['Zipcode']:
			zip_code = entry['Zipcode']
			#tmp_data['zip_code'] = entry['Zipcode']
		if entry['City']:
			tmp_data['city'] = entry['City']
		else:
			missing_data = True
		if entry['County']:
			tmp_data['county'] = entry['County']
		else:
			missing_data = True
		if entry['Longitude']:
			tmp_data['longitude'] = entry['Longitude']
		else:
			missing_data = True
		if entry['Latitude']:
			tmp_data['latitude'] = entry['Latitude']
		else:
			missing_data = True


		if not missing_data:

			if state in STATES_DICT:
				STATES_DICT[state][zip_code] = tmp_data
			else:
				STATES_DICT[state] = dict()
				STATES_DICT[state][zip_code] = tmp_data


def load_all_states():

	for state in STATES_LIST:
		load_state(state)


def create_directory(dir_name):

	try:
		os.makedirs(dir_name)
		print "Created %s" % dir_name
	except OSError, e:
		if e.errno == errno.EACCES:
			print "Could not create required directory %s" % dir_name
			sys.exit(1)
		else:
			print e
			sys.exit(1)


def validate_source_data():

	## Check for directories that store source data

	if not os.path.isdir(srcdir):
		print "Missing %s, exiting" % srcdir
		sys.exit(1)

	if not os.path.isdir(names_dir):
		print "Missing %s, exiting" % names_dir
		sys.exit(1)

	if not os.path.isdir(states_dir):
		# Okay if we are missing this, we can fetch new info
		create_directory(states_dir)

	if not os.path.isfile(streets_file):
		print "Cannot find %s, exiting" % streets_file
		sys.exit(1)


def print_state_info(state):

	print "Printing state info for %s" % state
	tmp_state = STATES_DICT[state]
	print str(tmp_state)


def count_zips():
	zip_count = 0
	for state in STATES_DICT:
		current_state = STATES_DICT[state]
		for zip in current_state:
			zip_count += 1
	return zip_count


def main():

	validate_source_data()
	load_all_names()
	load_all_states()
	load_streets()

	if args.verbose:
		print "Unique street names :%d" % len(STREETS_LIST)
		print "Unique zip codes    :%d" % count_zips()
		print "Number of patients  :%d" % args.patients
		print "Unique female names :%d" % count_female_names()
		print "Unique male names   :%d" % count_male_names()

	generate_patients(args.patients)


if __name__ == "__main__":
    main()
